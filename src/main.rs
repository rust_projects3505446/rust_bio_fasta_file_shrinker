use bio::io::fasta::{Reader,Writer};

fn main() -> std::io::Result<()> {
    // Define file paths
    let output_path = "../shrinked_sclerot.fasta"; // Replace with your desired output file path
    let mut writer = match Writer::to_file(output_path){
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
    // Open files
    let mut info = (match Reader::from_file("../s_sclerot.fa"){
        Ok(file) => file,
        Err(error) => panic!("{}",error),
        }).records();
    // Read and write FASTA elements
    for _i in 1..10{
        writer.write_record(&info.next().unwrap().unwrap()).unwrap();
        }
    println!("Successfully wrote the first 10 FASTA elements to {}", output_path);
    Ok(())
}

